import React, { useState, useEffect } from 'react';
import { Link } from 'gatsby';
import '../navstyle.css';
import { StaticImage } from "gatsby-plugin-image";
import Footer from './Footer';


const Layout = ({ children }) => {

  const [prevScrollpos, setPrevScrollpos] = useState(0);
  const [visible, setVisible] = useState(true);

  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      setVisible(prevScrollpos > currentScrollPos);
      setPrevScrollpos(currentScrollPos);
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [prevScrollpos]);

  return (
    <div>

      <div className={`navbar ${visible ? '' : 'hidden'} bg-white fixed top-0 w-full flex transition-transform shadow-md py-2`}>

        <div className="header-left flex justify-between" >
          <div>
            <Link to="#home">
              <StaticImage src="../images/Roberts-Logo.png" alt="My Image" className="w-20 ml-16" />
            </Link>
          </div>
          <div className="mt-5 mr-12 font-mono text-lg flex" >
            <Link className='pl-6 pr-5' to="/">Home</Link>
            <div className="relative group" onMouseEnter={() => setIsDropdownOpen(true)} onMouseLeave={() => setIsDropdownOpen(false)}>
              <Link className='pr-5' to="#news" >About Us</Link>
              <ul className={`absolute  bg-white mt-5 left-0 w-max p-4 rounded shadow-lg overflow-hidden transition-opacity duration-300 ${isDropdownOpen ? 'opacity-100 pointer-events-auto' : 'opacity-0 pointer-events-none'}`}
                // onMouseEnter={() => setIsDropdownOpen(true)}
                // onMouseLeave={() => setIsDropdownOpen(false)} 
                style={{ left: "-50px" }} >
                <li onMouseEnter={() => setIsDropdownOpen(true)}
                  className="mb-2"><Link to="/vision-and-mission" className="hover:text-blue-500 text-sm">Vison and Mission</Link></li>
                <li onMouseEnter={() => setIsDropdownOpen(true)}
                  className="mb-2"><Link to="/messages" className="hover:text-blue-500 text-sm">Messages</Link></li>
                <li onMouseEnter={() => setIsDropdownOpen(true)}
                  className="mb-2"><Link to="/eventstimeline" className="hover:text-blue-500 text-sm">Timeline of Important Events</Link></li>
              </ul>
            </div>

            <Link className='pr-5' to="#contact">Contact Us</Link>
          </div>


        </div>

      </div>

      {/* Site Header (you can customize this as per your design) */}
      <header>
        {children}
      </header>

      {/* Content of the current page */}
      <main ></main>

      <Footer />

      {/* Site Footer (you can customize this as per your design) */}
      <footer>

      </footer>
    </div>
  );
};

export default Layout;
