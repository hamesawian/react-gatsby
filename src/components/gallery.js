// import React from 'react';
// import { useStaticQuery, graphql } from 'gatsby';

// const Gallery = () => {
//   const data = useStaticQuery(graphql`
//     query MyQuery {
//       allNodeGallery {
//         nodes {
//           relationships {
//             field_paragraph_gallery {
//               relationships {
//                 field_upload_image {
//                   url
//                 }
//               }
//             }
//           }
//         }
//       }
//     }
//   `);

//   const images = data.allNodeGallery.nodes;

//   return (
//     <div>
//       {images.map((image, index) => (
//         <img
//           key={index}
//           src={image.relationships.field_paragraph_gallery.relationships.field_upload_image.url}
//           alt={`Image ${index + 1}`}
//         />
//       ))}
//     </div>
//   );
// };

// export default Gallery;
