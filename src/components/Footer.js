import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

const Footer = () => {
  const data = useStaticQuery(graphql`
    query MyQuery {
      allBlockContentBasic(filter: {id: {eq: "c81d3045-7f70-5d9a-a6d4-292113b37dd5"}}) {
        nodes {
          body {
            processed
          }
          id
        }
      }
    }
  `);

  const processed = data.allBlockContentBasic.nodes[0]?.body?.processed || '';

  return (
    <footer>
      <div dangerouslySetInnerHTML={{ __html: processed }} />
    </footer>
  );
};

export default Footer;
