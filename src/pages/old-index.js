// import React from 'react';
// import { graphql, useStaticQuery } from 'gatsby';
// import Layout from '../components/layout';

// const HomePage = ({data}) => {

//   console.log(data)

//   const leftBlockParagraphs = data.leftBlock?.nodes;
//   const centerBlockParagraphs = data.centerBlock?.nodes;
//   const rightBlockParagraphs = data.rightBlock?.nodes;

//   const nodes = data.allNodeHome.nodes;
//   const basicBlockNodes = data.allBlockContentBasic.nodes;
//   const firstTwoBasicBlockNodes = basicBlockNodes.slice(2);

//   return (
//     <Layout>
//       <div>
//         {nodes.map((node, index) => (
//           <div key={index}>
//             <img className='w-11/12 mx-auto mt-32' src={node.relationships.field_display_image?.url} alt={`Image ${index + 1}`} />
//             <div className='m-7 mx-14 text-2xl text-justify' dangerouslySetInnerHTML={{ __html: node.body?.processed }} />
//           </div>
//         ))}

//         <div className="allblocksection flex justify-between ml-20 mr-20 mt-8 mb-12">
//           {/* Render LeftBlock content */}
//           {leftBlockParagraphs.map((paragraph, index) => (
//             <div className=" mr-4" key={`left-block-${index}`}>
//               <div dangerouslySetInnerHTML={{ __html: paragraph.field_homeb?.processed }} />
//             </div>
//           ))}

//           {/* Render CenterBlock content */}
//           {centerBlockParagraphs.map((paragraph, index) => (
//             <div className=" mx-4" key={`center-block-${index}`}>
//               <div dangerouslySetInnerHTML={{ __html: paragraph.field_homeb?.processed }} />
//             </div>
//           ))}

//           {/* Render RightBlock content */}
//           {rightBlockParagraphs.map((paragraph, index) => (
//             <div className=" ml-4" key={`right-block-${index}`}>
//               <div dangerouslySetInnerHTML={{ __html: paragraph.field_homeb?.processed }} />
//             </div>
//           ))}
//         </div>

//         {firstTwoBasicBlockNodes.map((node, index) => (
//           <div key={`block-${index}`}>
//             <div dangerouslySetInnerHTML={{ __html: node.body?.processed }} />
//           </div>
//         ))}
//       </div>
//     </Layout>
//   );
// };

// export const data = graphql`
// query MyQuery {
//   allNodeHome {
//     nodes {
//       body {
//         processed
//       }
//       relationships {
//         field_display_image {
//           url
//         }
//       }
//     }
//   }

//   allBlockContentBasic {
//     nodes {
//       body {
//         processed
//       }
//     }
//   }

//   leftBlock: allParagraphHomePargraph(filter: { id: { eq: "cc806bd7-26d3-5acb-a5a4-2679a1da5d33" } }) {
//     nodes {
//       field_homeb {
//         processed
//       }
//       id
//     }
//   }

//   centerBlock: allParagraphHomePargraph(filter: { id: { eq: "3b29a752-7125-5fbe-8763-458085a85005" } }) {
//     nodes {
//       field_homeb {
//         processed
//       }
//       id
//     }
//   }

//   rightBlock: allParagraphHomePargraph(filter: { id: { eq: "f844ba9d-afee-5dab-806e-e03e36f78f67" } }) {
//     nodes {
//       field_homeb {
//         processed
//       }
//       id
//     }
//   }
// }

// `;

// export default HomePage;
