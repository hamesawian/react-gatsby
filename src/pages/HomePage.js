import React from "react";

const HomePage = ({ data }) => {
  // Extract the title from the data
  const title = data.allNodeHome.nodes[0].title;
  const id = data.allNodeHome.nodes[0].id;

  return (
    <div>
      <h1 style={{color:'red'}}>{title}</h1>
      <h2>{id}</h2>
      {/* Add other content or components as needed */}
    </div>
  );
};

export default HomePage;