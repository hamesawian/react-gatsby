import React from "react";

const VisionTemplate = ({ pageContext }) => {
    const { aboutUsData } = pageContext;
    console.log(aboutUsData);

    if (!aboutUsData) {
        // Handle case when data is not available yet
        return <div>Loading...</div>;
    }

    const {title, relationships} = aboutUsData;
    const fieldHomeTitle = relationships.field_body_paragraph[0].field_home_title?.processed;
    const fielHomebody = relationships.field_body_paragraph[0].field_homeb?.processed;

    const fieldHomeTitle2 = relationships.field_body_paragraph[1].field_home_title?.processed;
    const fielHomebody2 = relationships.field_body_paragraph[1].field_homeb?.processed;

    const fieldHomeTitle3 = relationships.field_body_paragraph[2].field_home_title?.processed;
    const fielHomebody3 = relationships.field_body_paragraph[2].field_homeb?.processed;
    console.log(fielHomebody);
   


    return (

    <div className=" pt-40 ml-16 mr-16">
        <div className=" mb-10">
        <h1 className=" font-extrabold text-4xl">{title}</h1>
        </div>

        <div className=" mb-10 text-xl">
        <h1 className=" font-extrabold mb-4">{fieldHomeTitle}</h1>
            <div dangerouslySetInnerHTML={{ __html: fielHomebody }}></div>
        </div>

        <div className=" mb-10 text-xl">
        <h1 className=" font-extrabold mb-4">{fieldHomeTitle2}</h1>
            <div dangerouslySetInnerHTML={{ __html: fielHomebody2 }}></div>
        </div>

        <div className=" mb-10 text-xl">
        <h1 className=" font-extrabold mb-4">{fieldHomeTitle3}</h1>
            <div dangerouslySetInnerHTML={{ __html: fielHomebody3 }}></div>
        </div>
    </div>
        
    )
   
};


export default VisionTemplate;
