import React from 'react';

const Events = ({ pageContext }) => {
  const { Eventsdata } = pageContext;

  if (!Eventsdata) {
    // Handle case when data is not available yet
    return <div>Loading...</div>;
  }

  const { title, field_event } = Eventsdata;
  const processedContent = field_event[0]?.processed || '';

  return (
    <div className="pt-40 ml-16 mr-16 pb-10">
     
      <div className="text-xl" dangerouslySetInnerHTML={{ __html: processedContent }} />
      {/* Add other components and styling as needed */}
    </div>
  );
};

export default Events;
