import React from 'react';

const MessagesTemplate = ({ pageContext }) => {
  const { Messagesdata } = pageContext;

  if (!Messagesdata) {
    // Handle case when data is not available yet
    return <div>Loading...</div>;
  }

  const { title, relationships } = Messagesdata;
  const gallery = relationships?.field_paragraph_gallery[0];
  const paragraphType = gallery?.relationships?.paragraph_type;
  const galleryData = paragraphType?.relationships?.paragraph__gallery || [];

  // Assuming you want to display the first image and its corresponding processed text
  const image = Messagesdata.relationships.field_paragraph_gallery[0].relationships.field_upload_image[0];
  const processedArray = galleryData[0]?.field_formated_long.map(item => item?.processed) || [];

  const image1 = Messagesdata.relationships.field_paragraph_gallery[0].relationships.field_upload_image[1];

  const image2 = Messagesdata.relationships.field_paragraph_gallery[0].relationships.field_upload_image[2];

  // Assuming you only want to display the first processed text
  const processed = processedArray[0];
  const processed1 = processedArray[1];
  const processed2 = processedArray[2];

  const processedtitle = galleryData[0]?.field_title_gallery.map(item => item?.processed) || [];
  
  const processedt = processedtitle[0];
  const processedt1 = processedtitle[1];
  const processedt2 = processedtitle[2];


  return (
    <div className="pt-40 ml-16 mr-16">
      <div className="mb-7">
        <h1 className="font-extrabold text-4xl">{title}</h1>
      </div>
      <div className="">
        <div className="flex mb-8">
          <img className="mb-8 w-52" src={image?.url} alt={`Image`} />
          <div className="grid">
          <div className="ml-10 text-2xl font-extrabold" dangerouslySetInnerHTML={{ __html: processedt }} />
          <div className="ml-10 mb-20 text-xl" dangerouslySetInnerHTML={{ __html: processed }} />
          </div>
        </div>

        <div className="flex mb-8">
          <img className="mb-8 w-52" src={image1?.url} alt={`Image1`} />
          <div className="grid">
          <div className="ml-10 text-2xl font-extrabold" dangerouslySetInnerHTML={{ __html: processedt1 }} />
          <div className="ml-10 mb-20 text-xl" dangerouslySetInnerHTML={{ __html: processed1 }} />
          </div>
        </div>

        <div className="flex mb-8">
          <img className="mb-8 w-52" src={image2?.url} alt={`Image2`} />
          <div className="grid">
          <div className="ml-10 text-2xl font-extrabold" dangerouslySetInnerHTML={{ __html: processedt2 }} />
          <div className="ml-10 mb-20 text-xl" dangerouslySetInnerHTML={{ __html: processed2 }} />
          </div>
        </div>

      </div>
    </div>
  );
};

export default MessagesTemplate;
