import React, { useState } from "react";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";

const HomeParagraphTemplate = ({ pageContext }) => {
  const { data, galleryData } = pageContext;

  const HomeMainHeading = data;
  console.log(HomeMainHeading);

  const { body, relationships } = HomeMainHeading;
  const { processed } = body;
  const imageUrl = relationships?.field_display_image?.url;

  const galleryUrls = galleryData.map((image) => image.url);

  const [lightboxIndex, setLightboxIndex] = useState(null);

  return (
    <div className="Homepage mx-10">
      {imageUrl && (
        <img src={imageUrl} alt="Display Image" className="w-11/12 mx-auto mt-32" />
      )}

      <div className="Block-area">
        {/* Display the processed content */}
        <div
          className="pl-14 pr-14 text-justify text-xl pt-14 pb-14 font-bold"
          dangerouslySetInnerHTML={{ __html: processed }}
        />
      </div>

      <div className="grid grid-cols-4 gap-4 p-14 border-t-2 border-gray-300">
        {galleryUrls.map((image, index) => (
          <img 
            key={index}
            src={image}
            alt={`Image ${index}`}
            className="cursor-pointer rounded-md border-b-4 border-blue-500  transform hover:scale-105 hover:opacity-75 transition-transform duration-300" 
            onClick={() => setLightboxIndex(index)}
          />
        ))}
      </div>

      {lightboxIndex !== null && (
        <Lightbox
          open={true}
          slides={galleryUrls.map((src) => ({ src }))}
          currentIndex={lightboxIndex}
          close={() => setLightboxIndex(null)}
        />
      )}
    </div>
  );
};

export default HomeParagraphTemplate;
