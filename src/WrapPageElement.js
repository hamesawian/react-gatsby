// src/WrapPageElement.js
import React, { useState, useEffect } from 'react';
import Layout from './components/layout';

const WrapPageElement = ({ element }) => {
  const [prevScrollpos, setPrevScrollpos] = useState(0);
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      setVisible(prevScrollpos > currentScrollPos);
      setPrevScrollpos(currentScrollPos);
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [prevScrollpos]);

  return <Layout visible={visible} setVisible={setVisible}>{element}</Layout>;
};

export default WrapPageElement;
