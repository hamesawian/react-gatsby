/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/reference/config-files/gatsby-browser/
 */

// You can delete this file if you're not using it
import './src/styles/global.css';
// gatsby-browser.js
import './src/tailwind.css';

// gatsby-browser.js
import React from 'react';
import WrapPageElement from './src/WrapPageElement';

export const wrapPageElement = ({ element }) => {
  return <WrapPageElement element={element} />;
};

