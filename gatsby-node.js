/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/
 */

/**
 * @type {import('gatsby').GatsbyNode['createPages']}
 * 
 *
 */

const path  = require("path");
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const homeParagraph = await graphql(`
    query {
      allParagraphHomePargraph(filter: { id: { eq: "cc806bd7-26d3-5acb-a5a4-2679a1da5d33" } }) {
        nodes {
          field_homeb {
            processed
          }
          id
        }
      } 
    }
  `);

  if (homeParagraph.errors) {
    throw homeParagraph.errors;
  }

  // createPage({
  //   path: "/using-dsg",
  //   component: require.resolve("./src/templates/using-dsg.js"),
  //   context: {},
  //   defer: true,
  // })

  // Extract the relevant data from the GraphQL result
  const HomeLeftBlock = homeParagraph.data.allParagraphHomePargraph.nodes[0];

  createPage({
    path: "/",
    component: require.resolve("./src/templates/home-paragraph.js"),
    context: {
      data: HomeLeftBlock,
    },
  });


  const HomeHeading = await graphql(`
  query AnotherQuery {
    allNodeHome {
      nodes {
        body {
          processed
        }
        relationships {
          field_display_image {
            url
          }
        }
      }
    }
    allBlockContentBasic {
      nodes {
        body {
          processed
        }
      }
    }

    allFileFile(skip: 6, limit: 7) {
      nodes {
        url
      }
    }
  }
`);

if (HomeHeading.errors) {
  throw HomeHeading.errors;
}

// Extract the relevant data from the GraphQL result for the other page
const HomeMainHeading = HomeHeading.data.allNodeHome.nodes[0];
const HomeGallery = HomeHeading.data.allFileFile.nodes;

// Create the other page (e.g., "/home-paragraph") with its unique data
createPage({
  path: "/",
  component: require.resolve("./src/templates/home-paragraph.js"),
  context: {
    data: HomeMainHeading,
    galleryData: HomeGallery,
  },
});


const aboutUsQuery = await graphql(`
query vision  {
  allNodeAboutUs(limit: 1, skip: 1) {
    nodes {
      title
      id
      relationships {
        field_body_paragraph {
          field_home_title {
            processed
          }
          field_homeb {
            processed
          }
        }
      }
    }
  }
}
`);

if (aboutUsQuery.errors) {
  throw aboutUsQuery.errors;
}

const aboutUsData = aboutUsQuery.data.allNodeAboutUs.nodes[0];

createPage({
  path: "/vision-and-mission", 
  component: require.resolve("./src/templates/vision-and-mission.js"),
  context: {
    aboutUsData,
  },
});


const resultMessage = await graphql(`
query {
  allNodeGallery {
    nodes {
      title
      relationships {
        field_paragraph_gallery {
          relationships {
            field_upload_image {
              url
            }
            paragraph_type {
              relationships {
                paragraph__gallery {
                  field_formated_long {
                    processed
                  }
                  field_title_gallery {
                    processed
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
`);


if (resultMessage.errors) {
  throw resultMessage.errors;
}

const Messagesdata = resultMessage.data.allNodeGallery.nodes[0];

  createPage({
    path: "/messages", 
    component: require.resolve("./src/templates/messages.js"),
    context: {
      Messagesdata,
    },
  });


  const resultEvent = await graphql(`
  query {
    allNodeNotice { 
      nodes {
        title
        field_event {
          processed
        }
      }
    }
  }
`);

if (resultEvent.errors) {
  throw resultEvent.errors;
}

const Eventsdata = resultEvent.data.allNodeNotice.nodes[0]; // Use the correct query data

createPage({
  path: "/eventstimeline",
  component: require.resolve("./src/templates/events.js"),
  context: {
    Eventsdata,
  },
});


};


